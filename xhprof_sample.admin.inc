<?php
/**
 * @file
 * Admin configuration forms and functionality for XHProf Sample module.
 */

/**
 * Form callback for XHProf sample admin settings.
 */
function xhprof_sample_admin_settings_form($form, &$form_state) {
  $form['xhprof_sample_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable sampling of requests.'),
    '#default_value' => variable_get('xhprof_sample_enabled', FALSE),
    '#description' => t('Enables sampling and collecting of the resulting data.'),
    '#disabled' => !extension_loaded('xhprof'),
  );

  $form['settings'] = array(
    '#title' => t('Sampling settings'),
    '#type' => 'fieldset',
    '#states' => array(
      'invisible' => array(
        'input[name="xhprof_sample_enabled"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['settings']['xhprof_sample_output_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Output Directory'),
    '#required' => TRUE,
    '#description' => t('Specify the output directory for sample files as a stream wrapper URI. <em>Note that using a public:// scheme for this setting may have security implications.</em>'),
    '#default_value' => variable_get('xhprof_sample_output_dir', XHPROF_SAMPLE_DEFAULT_OUTPUT_DIR),
  );

  $form['settings']['xhprof_sample_header_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Conditionally enable sampling using request header.'),
    '#description' => t('If enabled, requests will only be sampled when the X-XHProf-Sample-Enable header is supplied and set to a truthy value.'),
    '#default_value' => variable_get('xhprof_sample_header_enable', FALSE),
  );

  $form['settings']['xhprof_sample_path_enable_type'] = array(
    '#type' => 'radios',
    '#title' => t('Enable sampling of specific paths'),
    '#options' => array(
      XHPROF_SAMPLE_ENABLE_PATH_NOTLISTED => t('All paths except those listed'),
      XHPROF_SAMPLE_ENABLE_PATH_LISTED => t('Only the listed paths'),
    ),
    '#default_value' => variable_get('xhprof_sample_path_enable_type', XHPROF_SAMPLE_ENABLE_PATH_LISTED),
  );

  $description = t("Specify pages to enable sampling on by using their paths. Leave blank for all pages. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
    '%blog' => 'blog',
    '%blog-wildcard' => 'blog/*',
    '%front' => '<front>',
  ));
  $form['settings']['xhprof_sample_path_enable_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#description' => $description,
    '#default_value' => variable_get('xhprof_sample_path_enable_paths', ''),
  );

  $form['actions']['purge'] = array(
    '#type' => 'submit',
    '#value' => t('Purge XHProf Samples'),
    '#submit' => array('xhprof_sample_purge_samples'),
  );

  $form = system_settings_form($form);
  return $form;
}

/**
 * Validate callback for xhprof_sample_admin_settings_form.
 */
function xhprof_sample_admin_settings_form_validate(&$form, &$form_state) {
  $values =& $form_state['values'];

  // If a private scheme is specified, confirm the private filesystem
  // is configured and that the output directory can be written.
  if (file_uri_scheme($values['xhprof_sample_output_dir']) == 'private') {
    if (!variable_get('file_private_path', NULL) || !file_prepare_directory($values['xhprof_sample_output_dir'], FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      form_set_error('xhprof_sample_output_dir', t('The private filesystem is not configured. Configure it on the !link', array(
        '!link' => l(t('File System configuration page'), 'admin/config/media/file-system'),
      )));

      $form_state['values']['xhprof_sample_enabled'] = 0;
    }
  }
}

/**
 * Submit handler for cleanup of samples.
 */
function xhprof_sample_purge_samples() {
  $runclass = xhprof_sample_runclass();
  $count = $runclass::purge();

  drupal_set_message(t('Purged !count XHProf samples.', array('!count' => $count)));
}

/**
 * Page callback for outputting a list of sample files.
 */
function xhprof_sample_output_list() {
  module_load_include('inc', 'xhprof_sample');
  $runclass = xhprof_sample_runclass();
  $samples = $runclass::collectAll();

  $output = xhprof_sample_table($samples);
  $output .= theme('pager');

  return $output;
}
