<?php
/**
 * @file
 * Utility functions for XHProf Sample module.
 */

/**
 * Generates a table of sample information.
 *
 * @param array $samples
 *   An array of sample metadata.
 *
 * @return string
 *   A rendered table of sample information.
 */
function xhprof_sample_table($samples) {
  $rows = array();
  $header = array(
    array('data' => t('Path')),
    array('data' => t('Runtime'), 'field' => 'runtime', 'sort' => 'desc'),
    array('data' => t('User')),
    array('data' => t('Method')),
    array('data' => t('Operations')),
  );

  $sort = tablesort_get_sort($header);

  // TODO: this should eventually be handled in a standard way
  // directly in each run class.
  xhprof_sample_table_runtime_sort($samples, $sort);

  $page_samples = xhprof_sample_array_splice($samples, XHPROF_SAMPLE_OUTPUT_LIST_PER_PAGE);
  foreach ($page_samples as $uri => $meta) {
    $operation_links = xhprof_sample_run_operations($meta);
    $operations = theme('ctools_dropdown', array(
      'title' => 'Select',
      'links' => $operation_links,
    ));

    $rows[] = array(
      $meta['path'],
      $meta['runtime'],
      $meta['user'],
      $meta['method'],
      $operations,
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Gets a slice of the files array and sets up the global pager vars.
 */
function xhprof_sample_array_splice($data, $limit = 9, $element = 0) {
  global $pager_page_array, $pager_total, $pager_total_items;
  $page = isset($_GET['page']) ? $_GET['page'] : '';

  // Convert comma-separated $page to an array, used by other functions.
  $pager_page_array = explode(',', $page);

  // We calculate the total of pages as ceil(items / limit).
  $pager_total_items[$element] = count($data);
  $pager_total[$element] = ceil($pager_total_items[$element] / $limit);
  $pager_page_array[$element] = max(0, min((int) $pager_page_array[$element], ((int) $pager_total[$element]) - 1));

  return array_slice($data, $pager_page_array[$element] * $limit, $limit, TRUE);
}

/**
 * Sort a set of Sample by runtime.
 */
function xhprof_sample_table_runtime_sort(&$samples, $sort = 'desc') {
  usort($samples,
    function($a, $b) use ($sort) {
      switch ($sort) {
        case 'desc':
          return $b['runtime'] > $a['runtime'] ? 1 : -1;

        case 'asc':
          return $a['runtime'] > $b['runtime'] ? 1 : -1;

        default:
          return 1;
      }
    }
  );
}
